part of 'theme_bloc.dart';

@immutable
abstract class ThemeEvent extends Equatable {
  const ThemeEvent();
}

class Themechanged extends ThemeEvent {
  const Themechanged({@required this.theme});
  final AppTheme theme;

  @override
  List<Object> get props => [theme];
}
