import 'package:flutter/widgets.dart';
import 'package:myapp/core/localization/app_localizations.dart';

///This class provides an abstraction over AppLocalizations or whichever
///localization system will be used in order to have flexibility and keep
///the code concise
class Localized {
  ///Returns the localized string by accessing the localizations class.
  static String translate(BuildContext context, String s) =>
      AppLocalizations.translateString(context, s);
}
