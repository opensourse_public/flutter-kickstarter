import 'package:sailor/sailor.dart';

import '../../features/feautre1/presentation/pages/homePage/home_page.dart';
import '../../features/feautre1/presentation/pages/preferencesPage/preferences_page.dart';

const String homeRoutePath = "/";
const String settingsPagePath = "/settings";

class Routes {
  static final Sailor sailor = Sailor();

  ///Creates the routes that will be used across all the app
  static void createRoutes() {
    sailor.addRoutes([
      SailorRoute(
        name: homeRoutePath,
        builder: (context, args, params) => MyHomePage(args as HomePageArgs),
      ),
      SailorRoute(
        name: settingsPagePath,
        builder: (context, args, params) => const SettingsPage(),
      ),
    ]);
  }
}
